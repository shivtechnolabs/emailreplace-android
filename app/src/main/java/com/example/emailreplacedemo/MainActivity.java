package com.example.emailreplacedemo;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;

public class MainActivity extends AppCompatActivity {

    private EditText etEmailAddress;
    private Button btnSubmit;
    private TextView tvReplace,tvReplaceEmailWord;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialization();
        idMapping();
        addTextWatcher();
        setEventListener();
    }


    /**
     *  add TExt Watcher
     */
    private void addTextWatcher() {
        etEmailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvReplace.setVisibility(View.GONE);
                tvReplaceEmailWord.setText("");
                tvReplaceEmailWord.setVisibility(View.GONE);
            }
            @Override
            public void afterTextChanged(Editable s) {



            }
        });
    }

    private void initialization() {
        context=this;
    }

    private void idMapping() {

        etEmailAddress = (EditText) findViewById(R.id.etEmailAddress);
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        tvReplace = (TextView) findViewById(R.id.tvReplace);
        tvReplace.setVisibility(View.GONE);
        tvReplaceEmailWord = (TextView) findViewById(R.id.tvReplaceEmailWord);
    }



    /**
     *  handle Click Event
     */
    private void setEventListener() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if(infoValid()){
                    tvReplace.setVisibility(View.VISIBLE);
                    tvReplace.setText(R.string.output);
                    tvReplaceEmailWord.setVisibility(View.VISIBLE);
                    String emailsNewString = getEmailsNewString(etEmailAddress.getText().toString());
                    tvReplaceEmailWord.setText(emailsNewString);

                }
            }
        });
    }


    /**
     *  check For Valid Input
     * @return
     */
    private boolean infoValid(){
        if(etEmailAddress.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(context, R.string.please_enter_input,Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    /**
     *  get Replace New Email String
     * @param input
     * @return
     */
    public  String  getEmailsNewString(@NonNull String input) {
//        input="Christian has the email christian+123@gmail.com. Christian friend Lars-Ole Jensen has the email lars-ole.jensen@gmail.com. Lars-Oles daughter Britt studies at Oxford University and has the email britt123@oxford.co. uk.";
        StringBuilder stringBuilder=new StringBuilder(input);
        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(input);
        while (matcher.find()) {
            int matchStart = matcher.start(0);
            int matchEnd = matcher.end(0);
            String detectEmailAddress = stringBuilder.substring(matchStart, matchEnd);
            replaceEmailAddress(detectEmailAddress);
            stringBuilder.replace(matchStart,matchEnd,replaceEmailAddress(detectEmailAddress));


            /**
             *  change Matcher
             */
            matcher=Patterns.EMAIL_ADDRESS.matcher(stringBuilder.toString());
        }
        return stringBuilder.toString();
    }

    /**
     *  In String change Data @ with [AT],
     *  . with [DOT]
     * @param replaceEmail
     * @return
     */
    private String replaceEmailAddress(String replaceEmail){
        String[] pieces = replaceEmail.split("@");
        StringBuilder stringBuilder=new StringBuilder();
        if(pieces.length >=1){
            stringBuilder.append(pieces[0]).append("[AT]");
            String[] splitDot = pieces[1].split("\\.");
            int length = splitDot.length;
            for (int i=0;i<length;i++ ){
                if(i==length-1){
                    stringBuilder.append(splitDot[i]);
                }else {
                    stringBuilder.append(splitDot[i]).append("[DOT]");
                }
            }
        return stringBuilder.toString();
        }

         return replaceEmail;

    }
}
